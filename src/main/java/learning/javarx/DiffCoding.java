package learning.javarx;

import java.util.Arrays;
import java.util.List;

public class DiffCoding {
	
	
	public static void main(String[] args){
		List<Integer> data = Arrays.asList(5,3,3,4,1);
		DiffCodingUtils.leftDiffCoding(data, (a,b) -> a-b).forEach(result -> System.out.print(result + " "));
		System.out.println();
		DiffCodingUtils.rightDiffCoding(data, (a,b) -> a-b).forEach(result -> System.out.print(result + " "));
	}
}
